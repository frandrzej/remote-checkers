
#ifndef _structures_h_included_
#define _structures_h_included_

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

#define kBOARD_SIZE 8
#define kMAX_PLAYER_MSG_SIZE 100 //chars



#define kMAX_PLAYER_NAME_LEN 30
#define kMAX_PLAYER_ID_LEN 30
#define kMAX_GAME_ID_LEN 8


typedef enum {
	//---General
    NickRegister = 1, //identify himself with nick (no passwords) 
    GameConnect, //connect to the game of given id (only, if he is the player in this game)
    ListGames, //list all games connected to him (by nick)
    RequestNewGame, //start a new game, id and status of a new game is sent to the player
		WaitForOpponent, //if there is no other new game on the server new game with status "waiting for opponent" is started
		JoinedGame, //if there is a new game without a opponent, the player joins this game
	
	//---Game specific
    BoardDisplay, //display the board
    GameStatus, //check game status (active,resolved, waiting for opponent)
    TurnCheck, //check the turn (mine,opponents)
    GameMoves, //list all moves made
    SendMsg, //leave any number of messages to the opponent
    ReadMsg, //read messages from the opponent
    MakeMove, //make move in form of coordinates e.g. b6 to c6
    GiveUp, //give up = lose and end game
    
  //---Statuses
    Error,
    NotFound,
    OKStatus

} Command;



typedef struct {
   int id;
   int status;
   const char* pl0Name;
   const char* pl1Name;
   int turn; // 0 = first player, 1 = second player
   const char** player0Messages;
   const char** player1Messages;
   
} Game;


typedef struct {
	int fromX;
	int fromY;
	int toX;
	int toY;
	
} Move;


//struct Player {
	//char *name; //name=(char *)malloc(20*sizeof(char));
//}

#endif
