
#ifndef _helpers_h_included_
#define _helpers_h_included_

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h> 

#include "structures.h"
#include "common.h"

#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))

#define HERR(source) (fprintf(stderr,"%s(%d) at %s:%d\n",source,h_errno,__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))


int ExtractCommand(char *response)
{
	char commandStr[kCOMMAND_INDICATOR_LEN+1];
	strncpy(commandStr, response, kCOMMAND_INDICATOR_LEN);
	commandStr[kCOMMAND_INDICATOR_LEN] = '\0';
	return atoi(commandStr);
}


const char* SerializeCommand(char* str, Command command)
{
	if(kCOMMAND_INDICATOR_LEN != 2)
		HERR("SerializeCommand unimplemented");

	if(command < 10) {
		sprintf(str, "%d%d", 0, command);
	} else if(command < 100){
		sprintf(str, "%d", command);
	} else
		HERR("SerializeCommand:");

	return str;
}



const char* CommandToString(Command command)
{
	switch(command) {
		case NickRegister: return "Register with a nick";
		case ListGames: return "List active games";
		case GameConnect: return "Connect to a game with a ID";
		case RequestNewGame: return "Request a new game";
		case WaitForOpponent: return "Wait for an opponent"; //server -> client
		case JoinedGame: return "Joined a game";
		case BoardDisplay: return "Display a game board";
		case GameStatus: return "Check game status";
		case TurnCheck: return "Check who's the turn";
		case GameMoves: return "List  all made moves";
		case SendMsg: return "Send message to opponent";
		case ReadMsg: return "Read messages from the opponent";
		case MakeMove: return "Move";
		case GiveUp: return "Give up";
		default: HERR("Wrong command type"); return "";
	}
}


int** newGameBoard()
{
	// xxxxxxxx = 1
	// xxxxxxxx
	// ________ = 0
	// ________
	// ________
	// ________
	// aaaaaaaa = 2
	// aaaaaaaa
	int i, j;
	
	int** board = malloc(kBOARD_SIZE * sizeof(int *));
	for(i = 0; i < kBOARD_SIZE ; i++)
		(*board)[i] = (int*)malloc(kBOARD_SIZE * sizeof(int));
	
	//int** board = malloc(kBOARD_SIZE * sizeof(int *));
	//int* values = calloc(kBOARD_SIZE*kBOARD_SIZE, sizeof(int));
	//int i, j;
	//for(i = 0 ; i < kBOARD_SIZE ; i++) {
		//board[i] = values + kBOARD_SIZE*kBOARD_SIZE;
	//}
	
	
	for(i = 0 ; i < kBOARD_SIZE ; i++) {
		for(j = 0 ; j < kBOARD_SIZE ; j++) {
			if(i == 0 || i == 1)
				board[i][j] = 1;
			else if(i == (kBOARD_SIZE-2) || i == (kBOARD_SIZE -1))
				board[i][j] = 2;
			else
				board[i][j] = 0;
		}
	}
	
	return board;
}

void destroyBoard(int** board)
{
	free(*board);
	free(board);
}


const char* serializeBoard(int** board)
{
	return "";
}




unsigned long hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;
    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}


char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp) {
        if (a_delim == *tmp) {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;
    result = malloc(sizeof(char*) * count);

    if (result) {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token) {
        	if(idx >= count)
        		HERR("str_plit");
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        if(!(idx == count - 1))
        		HERR("str_plit");
        *(result + idx) = 0;
    }

    return result;
}



#define GET_LINE_OK       0
#define GET_LINE_NO_INPUT 1
#define GET_LINE_TOO_LONG 2


static int getLine (char *prmpt, char *buff, size_t sz) {
    int ch, extra;

    // Get line with buffer overrun protection.
    if (prmpt != NULL) {
        printf ("%s", prmpt);
        fflush (stdout);
    }
    if (fgets (buff, sz, stdin) == NULL)
        return GET_LINE_NO_INPUT;

    // If it was too long, there'll be no newline. In that case, we flush
    // to end of line so that excess doesn't affect the next call.
    if (buff[strlen(buff)-1] != '\n') {
        extra = 0;
        while (((ch = getchar()) != '\n') && (ch != EOF))
            extra = 1;
        return (extra == 1) ? GET_LINE_TOO_LONG : GET_LINE_OK;
    }

    // Otherwise remove newline and give string back to caller.
    buff[strlen(buff)-1] = '\0';
    return GET_LINE_OK;
}




#endif
