#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>

#include "structures.h"
#include "helpers.h"
#include "common.h"


typedef enum {
	InMainMenu = 0,
	InGame
} ClientMode;

int clientMode;


//////////////////////////////////////////////////
//  Common
//////////////////////////////////////////////////



void usage(char * name)
{
	fprintf(stderr,"USAGE: %s\n1. %s with no parameters - using localhost and port 2000. \n2. %s server host \n", 
		name, name, name);
}

//Communication

int make_socket(void)
{
	int sock;
	sock = socket(PF_INET,SOCK_STREAM,0);
	if(sock < 0) 
		ERR("socket");
	return sock;
}

struct sockaddr_in make_address(char *address, uint16_t port)
{
	struct sockaddr_in addr;
	struct hostent *hostinfo;
	addr.sin_family = AF_INET;
	addr.sin_port = htons (port);
	hostinfo = gethostbyname(address);
	if(hostinfo == NULL) HERR("gethostbyname");
	addr.sin_addr = *(struct in_addr*) hostinfo->h_addr;
	return addr;
}


int connect_socket(char *name, uint16_t port)
{
	struct sockaddr_in addr;
	int socketfd;
	socketfd = make_socket();
	addr=make_address(name,port);
	if(connect(socketfd,(struct sockaddr*) &addr,sizeof(struct sockaddr_in)) < 0){
		if(errno!=EINTR) 
			ERR("connect");
		else { 
			//this is the same
			fd_set wfds ;
			int status;
			socklen_t size = sizeof(int);
			FD_ZERO(&wfds);
			FD_SET(socketfd, &wfds);
			if(TEMP_FAILURE_RETRY(select(socketfd+1,NULL,&wfds,NULL,NULL))<0) ERR("select");
			if(getsockopt(socketfd,SOL_SOCKET,SO_ERROR,&status,&size)<0) ERR("getsockopt");
			if(0!=status) ERR("connect");
		}
	}
	
	return socketfd;
}


void sendAndReceive(int fd, char data[kMAX_MSG_LEN])
{
	//Broken PIPE is treated as critical error here
	if(bulk_write(fd,(char *)data, kMAX_MSG_LEN) < 0 ) 
		ERR("write:");

	if(bulk_read(fd, (char *)data, kMAX_MSG_LEN) < 0 ) //< kMAX_MSG_LEN
		ERR("read:");
}

//////////////////////////////////////////////////
//  Communication bridge - process response
//////////////////////////////////////////////////

/*
	//---General
    NickRegister = 1, //identify himself with nick (no passwords) 
    GameConnect, //connect to the game of given id (only, if he is the player in this game)
    ListGames, //list all games connected to him (by nick)
    RequestNewGame, //start a new game, id and status of a new game is sent to the player
		WaitForOpponent, //if there is no other new game on the server new game with status "waiting for opponent" is started
		JoinedGame, //if there is a new game without a opponent, the player joins this game
	
	//---Game specific
    BoardDisplay, //display the board
    GameStatus, //check game status (active,resolved, waiting for opponent)
    TurnCheck, //check the turn (mine,opponents)
    GameMoves, //list all moves made
    SendMsg, //leave any number of messages to the opponent
    ReadMsg, //read messages from the opponent
    MakeMove, //make move in form of coordinates e.g. b6 to c6
    GiveUp, //give up = lose and end game
    
  //---Statuses
    Error,
    NotFound,
    OKStatus
*/


void process_response(char data[kMAX_MSG_LEN])
{
	printf("\nReceived: %s\n", data);
	int command = ExtractCommand(data);
	printf("Command [%d]: %s\n", command, CommandToString(command));

	switch(command) {
		case NickRegister: break; //nothing to do
		case GameConnect: 
			clientMode = InGame;

			//get the response status (ok/not found/error)
			//if ok - get the id and change mode to InGame
			break;
		case GiveUp:
			clientMode = InMainMenu;
		default:
			break;
			//if(DEBUG) HERR("Client process_response - unrecognized command");
	}

	return;
}


//////////////////////////////////////////////////
//  Communication bridge - prepare requests
//////////////////////////////////////////////////


//main menu

int prepare_NickRegister(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, NickRegister);

	//strcpy first (initialize), strcat then to add more substrings
	strcpy(data, commandPrefix);

	int promptResult;
    char buff[kMAX_PLAYER_NAME_LEN];

    printf ("\nIdentify yourself (max %d chars):\n", kMAX_PLAYER_NAME_LEN);    
    promptResult = getLine("> ", buff, sizeof(buff));

    if (promptResult == GET_LINE_NO_INPUT) {
        // Extra NL since my system doesn't output that on EOF.
        printf ("\nError no input\n");
        return -1;
    } else if (promptResult == GET_LINE_TOO_LONG) {
        printf ("Input too long [%s]\n", buff);
        return -1;
    }

    strcat(data, kDELIMITER); //, kDELIMITER_SIZE);
    strcat(data, buff);

    return 1;
}

int prepare_RequestNewGame(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, RequestNewGame);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_ListGames(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, ListGames);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_GameConnect(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, GameConnect);

	//strcpy first (initialize), strcat then to add more substrings
	strcpy(data, commandPrefix);

	int promptResult;
    char buff[kMAX_PLAYER_NAME_LEN];

    printf ("\nSpecify ID of the game (max %d chars):\n", kMAX_GAME_ID_LEN);    
    promptResult = getLine("> ", buff, sizeof(buff));

    if (promptResult == GET_LINE_NO_INPUT) {
        // Extra NL since my system doesn't output that on EOF.
        printf ("\nError no input\n");
        return -1;
    } else if (promptResult == GET_LINE_TOO_LONG) {
        printf ("Input too long [%s]\n", buff);
        return -1;
    }

    strcat(data, kDELIMITER); //, kDELIMITER_SIZE);
    strcat(data, buff);

    return 1;
}


//in game

int prepare_BoardDisplay(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, BoardDisplay);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_GameStatus(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, GameStatus);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_TurnCheck(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, TurnCheck);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_GameMoves(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, GameMoves);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_SendMsg(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, SendMsg);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_ReadMsg(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, ReadMsg);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_MakeMove(char data[kMAX_MSG_LEN])
{
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, MakeMove);
	strcpy(data, commandPrefix);
    return 1;
}

int prepare_GiveUp(char data[kMAX_MSG_LEN])
{
	printf("prepare give up\n");
	char commandPrefix[kCOMMAND_INDICATOR_LEN];
	SerializeCommand(commandPrefix, GiveUp);
	strcpy(data, commandPrefix);
    return 1;
}


int prepare_request(Command command, char data[kMAX_MSG_LEN]) 
{
	printf("--Request for command [%d] ", command);
	printf("%s: ", CommandToString(command));

	int returnValue;
	switch(command) {
		case NickRegister:
			returnValue = prepare_NickRegister(data);
			break;
		case ListGames:
			returnValue = prepare_ListGames(data);
			break;
		case RequestNewGame:
			returnValue = prepare_RequestNewGame(data);
			break;
		case GameConnect:
			returnValue = prepare_GameConnect(data);
			break;

		//in game
		case BoardDisplay:
			returnValue = prepare_BoardDisplay(data);
			break;
		case GameStatus:
			returnValue = prepare_GameStatus(data);
			break;
		case TurnCheck:
			returnValue = prepare_TurnCheck(data);
			break;
		case GameMoves:
			returnValue = prepare_GameMoves(data);
			break;
		case SendMsg:
			returnValue = prepare_SendMsg(data);
			break;
		case ReadMsg:
			returnValue = prepare_ReadMsg(data);
			break;
		case MakeMove:
			returnValue = prepare_MakeMove(data);
			break;
		case GiveUp:
			returnValue = prepare_GiveUp(data);
			break;
		default:
			if(DEBUG) HERR("prepare_request: unrecognized command");
			return -1;
	}

	//add eof tag
	addEOFTag(data);
	printf("%s\n", data);

	return returnValue;
}

//////////////////////////////////////////////////
//  User interface
//////////////////////////////////////////////////



int promptUserInGame()
{
	int promptResult;
    char buff[2];
    
    printf ("\nPossible actions:\n");    
    printf("1. %s\n", CommandToString(BoardDisplay));
    printf("2. %s\n", CommandToString(GameStatus));
    printf("3. %s\n", CommandToString(TurnCheck));
    printf("4. %s\n", CommandToString(GameMoves));
    printf("5. %s\n", CommandToString(SendMsg));
    printf("6. %s\n", CommandToString(ReadMsg));
    printf("7. %s\n", CommandToString(MakeMove));
    printf("8. %s\n", CommandToString(GiveUp));

    promptResult = getLine("Enter action number> ", buff, sizeof(buff));
    if (promptResult == GET_LINE_NO_INPUT) {
        // Extra NL since my system doesn't output that on EOF.
        printf ("\nNo input\n");
        return promptUserInGame();
    } else if (promptResult == GET_LINE_TOO_LONG) {
        printf ("Input too long [%s]\n", buff);
        return promptUserInGame();
    }
    
    int result = -1;
    int number = atoi(buff);
    switch(number){
		case 1: result = BoardDisplay; break;
		case 2: result = GameStatus; break;
		case 3: result = TurnCheck; break;
		case 4: result = GameMoves; break;
		case 5: result = SendMsg; break;
		case 6: result = ReadMsg; break;
		case 7: result = MakeMove; break;
		case 8: result = GiveUp; break;
		default: result = -1;
	}
	
	if(result < 0) { printf("Wrong selection.\n"); return promptUserInGame(); }
	else printf("Picked: %s\n", CommandToString(result));
 	
    return result;
}


void inGameAction(char *server, uint16_t port)
{
	char data[kMAX_MSG_LEN];
	memset(&data[0], 0, sizeof(data));

	Command command = -1;
	while((command = promptUserInGame()) < 0);
	while(!prepare_request(command, data));
 	
 	int fd = connect_socket(server,port);
	sendAndReceive(fd, data);
	if(TEMP_FAILURE_RETRY(close(fd))<0)
		ERR("close");

	process_response(data);
}


int promptUserOnConnect()
{	
	int promptResult;
    char buff[2];
    
    printf ("\nPossible actions:\n");    
    printf("1. %s\n", CommandToString(NickRegister));
    printf("2. %s\n", CommandToString(ListGames));
    printf("3. %s\n", CommandToString(RequestNewGame));
    printf("4. %s\n", CommandToString(GameConnect));

    promptResult = getLine("Enter action number> ", buff, sizeof(buff));
    if (promptResult == GET_LINE_NO_INPUT) {
        // Extra NL since my system doesn't output that on EOF.
        printf ("\nNo input\n");
        return promptUserOnConnect();
    } else if (promptResult == GET_LINE_TOO_LONG) {
        printf ("Input too long [%s]\n", buff);
        return promptUserOnConnect();
    }
    
    int result = -1;
    int number = atoi(buff);

    switch(number){
		case 1: result = NickRegister; break;
		case 2: result = ListGames; break;
		case 3: result = RequestNewGame; break;
		case 4: result = GameConnect; break;
		default:
			result = -1;
	}
	
	if(result < 0) { printf("Wrong selection.\n"); return promptUserOnConnect(); }
	else printf("Picked: %s\n", CommandToString(result));
 	 
    return result;
}


void inMainMenuAction(char *server, uint16_t port)
{
	char data[kMAX_MSG_LEN];
	memset(&data[0], 0, sizeof(data));

	Command command = -1;
	while((command = promptUserOnConnect()) < 0);
	while(!prepare_request(command, data));
 	
 	int fd = connect_socket(server,port);
	sendAndReceive(fd, data);
	if(TEMP_FAILURE_RETRY(close(fd))<0)
		ERR("close");

	process_response(data); 
}

//////////////////////////////////////////////////
//  Main
//////////////////////////////////////////////////


int main(int argc, char** argv) 
{
	char *server = "localhost";
	uint16_t port = 2000;

	if(argc==3) {
		server = argv[1];
		port = atoi(argv[2]);

	} else if(argc != 1) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if(sethandler(SIG_IGN,SIGPIPE)) 
		ERR("Seting SIGPIPE: ");
	
	clientMode = InMainMenu;
	while(1) {
		switch(clientMode) {
			case InMainMenu:
				printf("\n--- Main menu ---\n");
				inMainMenuAction(server, port);
				break;
			case InGame:
				printf("\n--- Game menu ---\n");
				inGameAction(server, port);
				break;
			default: HERR("Unspecified client mode");
		}
	}
	
	return EXIT_SUCCESS;
	
}


