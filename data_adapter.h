
#ifndef _data-adapters_h_included_
#define _data-adapters_h_included_

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h> 

#include "structures.h"
#include "common.h"
#include "helpers.h"


#define kUSERS_FILENAME "users.txt"
#define kGAMES_FILENAME "games.txt"

static long tempUsrIDCount = 0;
static long tempGameIDCount = 0;


/*
    //Create input file descriptor 
    input_fd = open (kUSERS_FILENAME, O_RDONLY);
    if (input_fd == -1) {
            perror ("open");
            return 2;
    }
*/

int saveUser(int ID, char* name)
{
	int output_fd;
    /* Create output file descriptor */
    output_fd = open(kUSERS_FILENAME, O_WRONLY | O_CREAT, 0644);
    if(output_fd == -1){
        perror("open");
        return 3;
    }

    char buffer[kMAX_PLAYER_NAME_LEN+kMAX_PLAYER_ID_LEN];
    strcpy(buffer, name);
    strcat(buffer, ID);
    strcat(buffer, "\n");

    ssize_t ret_out; //Number of bytes returned by read() and write()
    ret_out = write (output_fd, &buffer, (ssize_t) kMAX_PLAYER_NAME_LEN+kMAX_PLAYER_ID_LEN);
    if(ret_out != kMAX_PLAYER_NAME_LEN+kMAX_PLAYER_ID_LEN){
        perror("write");
        return -1;
    }
 
    close (output_fd); 
}



//temps

int nextUserID()
{
	return tempUsrIDCount++;
}


int nextGameID()
{
	//TODO
	return tempGameIDCount++;
}


int IDForUsername(char* username)
{
	//TODO:
	//- search in file for username string
	//- if found, get his id

	return nextUserID();
}




#endif