#ifndef _common_h_included_
#define _common_h_included_

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>

#include "structures.h"
#include "helpers.h"


#define DEBUG 1


#define kMAX_MSG_LEN 256
#define kCOMMAND_INDICATOR_LEN 2
#define kMSG_SIZE_INDICATOR_LEN 4

#define kEOF_TAG "<EOF>"
#define kEOF_TAG_SIZE 5

#define kDELIMITER_SIZE 1
#define kDELIMITER ";"


void addEOFTag(char msg[])
{
	strncat(msg, kDELIMITER, kDELIMITER_SIZE);
	//strncat(msg, kEOF_TAG, kEOF_TAG_SIZE);
	//TODO: check for size of the msg
}




int sethandler( void (*f)(int), int sigNo) {
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}


ssize_t bulk_read(int fd, char *buf, size_t count){
	int c;
	size_t len=0;
	do{
		c=TEMP_FAILURE_RETRY(read(fd,buf,count));
		if(c<0) return c;
		if(0==c) return len;
		buf+=c;
		len+=c;
		count-=c;
	}while(count>0);
	return len ;
}

ssize_t bulk_write(int fd, char *buf, size_t count){
	int c;
	size_t len=0;
	do{
		c=TEMP_FAILURE_RETRY(write(fd,buf,count));
		if(c<0) return c;
		buf+=c;
		len+=c;
		count-=c;
	}while(count>0);
	return len ;
}


#endif