
 #define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>

#include "structures.h" 
#include "helpers.h"
#include "common.h"
#include "data_adapter.h"

#define BACKLOG 3
 
volatile sig_atomic_t do_work=1 ;

//////////////////////////////////////////////////
//  Common
//////////////////////////////////////////////////

void usage(char * name)
{
	fprintf(stderr,"USAGE: %s\n1. %s with no parameters - using port 2000. \n2. %s port \n", 
		name, name, name);
}

void sigint_handler(int sig) 
{
	do_work=0;
}

int make_socket(int domain, int type)
{
	int sock;
	sock = socket(domain,type,0);
	if(sock < 0) ERR("socket");
	return sock;
}

int bind_tcp_socket(uint16_t port)
{
	struct sockaddr_in addr;
	int socketfd,t=1;
	socketfd = make_socket(PF_INET,SOCK_STREAM);
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR,&t, sizeof(t))) ERR("setsockopt");
	if(bind(socketfd,(struct sockaddr*) &addr,sizeof(addr)) < 0)  ERR("bind");
	if(listen(socketfd, BACKLOG) < 0) ERR("listen");
	return socketfd;
}

int add_new_client(int sfd)
{
	int nfd;
	if( (nfd = TEMP_FAILURE_RETRY(accept(sfd, NULL, NULL))) < 0) {
		if(EAGAIN == errno || EWOULDBLOCK == errno)
			return -1;
		ERR("accept");
	}
	return nfd;
}




//////////////////////////////////////////////////
//  Processing requests
//////////////////////////////////////////////////

void process_NickRegister(char data[kMAX_MSG_LEN])
{
	char** tokens = str_split(data, kDELIMITER[0]);
	if(!tokens)
		HERR("Wrong data after string splitting");

	int i;
	char username[kMAX_PLAYER_NAME_LEN];

    for (i = 0; *(tokens + i); i++) {
        //printf("[%s]\n", *(tokens + i));
        strcpy(username, *(tokens + i));
        free(*(tokens + i));
    }


    printf("Saving user [%s] with ID [%d]\n", username, IDForUsername(username));
    free(tokens);
}

void process_GameConnect(char data[kMAX_MSG_LEN])
{
	char** tokens = str_split(data, kDELIMITER[0]);
	if(!tokens)
		HERR("Wrong data after string splitting");

	int i;
	char gameID[kMAX_GAME_ID_LEN];

    for (i = 0; *(tokens + i); i++) {
        // printf("[%s]\n", *(tokens + i));
        strcpy(gameID, *(tokens + i));
        free(*(tokens + i));
    }

	printf("Creating game with ID [%s]\n", gameID);
    free(tokens);
}



//////////////////////////////////////////////////
//  Communication bridge
//////////////////////////////////////////////////

void processMessage(char data[kMAX_MSG_LEN])
{
	printf("Received: %s\n", data);

	int command = ExtractCommand(data);
	printf("--Requested command: %s\n", CommandToString(command));

	switch(command) {
		case NickRegister:
			process_NickRegister(data);
			break;
		default: return;
	}
}


void communicate(int cfd)
{
	ssize_t size;
	char data[kMAX_MSG_LEN];
	if((size=bulk_read(cfd,(char *)data, kMAX_MSG_LEN))<0) 
		ERR("read:");
		
	if(size == kMAX_MSG_LEN) {
		processMessage(data);
		if(bulk_write(cfd, (char *)data, kMAX_MSG_LEN) < 0 && errno != EPIPE)
			ERR("write:");
	} else
		printf("Client disconnected on read\n");
	
	if(TEMP_FAILURE_RETRY(close(cfd))<0)
		ERR("close");
}


void doServer(int fdT)
{
	int cfd;
	fd_set base_rfds, rfds ;
	sigset_t mask, oldmask;
	FD_ZERO(&base_rfds);
	FD_SET(fdT, &base_rfds);
	
	sigemptyset (&mask);
	sigaddset (&mask, SIGINT);
	sigprocmask (SIG_BLOCK, &mask, &oldmask);
	
	while(do_work) {

		rfds = base_rfds;
		if( pselect(fdT+1, &rfds, NULL, NULL, NULL, &oldmask) > 0 ) {
			cfd = add_new_client(fdT);
			if(cfd >= 0)
				communicate(cfd);

		} else {
			if(EINTR == errno) 
				continue;
			ERR("pselect");
		}
	}
	
	sigprocmask (SIG_UNBLOCK, &mask, NULL);
}



//////////////////////////////////////////////////
//  Main
//////////////////////////////////////////////////


int main(int argc, char** argv) 
{
	uint16_t port = 2000;
	if(argc == 2) {
		port = atoi(argv[2]);

	} else if(argc != 1) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}


	int fdT;
	int new_flags;

	if(sethandler(SIG_IGN,SIGPIPE))
		ERR("Seting SIGPIPE:");
	if(sethandler(sigint_handler,SIGINT))
		ERR("Seting SIGINT:");
	
	fdT=bind_tcp_socket(port);
	new_flags = fcntl(fdT, F_GETFL) | O_NONBLOCK;
	fcntl(fdT, F_SETFL, new_flags);
	
	doServer(fdT);
        
	if(TEMP_FAILURE_RETRY(close(fdT))<0)
			ERR("close");
	
	fprintf(stderr,"Server has terminated.\n");
	
	return EXIT_SUCCESS;
}
